module.exports = function (app) {
  app.get("/", (req, res) => {
    res.json({ message: "App is running..." });
  });

  require("./auth.routes")(app);
  require("./user.routes")(app);
  require("./role.routes")(app);
};
